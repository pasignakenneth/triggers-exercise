﻿using System;
using Xamarin.Forms;

namespace TriggersExercise
{
    public class StartButtonTrigger : TriggerAction<Button>
    {
        protected override void Invoke(Button button)
        {
            if (button.Text == "START")
            {
                button.Text = "ARRIVED";
            }
            else if(button.Text== "ARRIVED")
            {
                button.IsVisible = false;
                button.IsEnabled = false;
                button.StyleId = "2";
            }

        }
    }
}
