﻿using System;
using Xamarin.Forms;

namespace TriggersExercise
{
    public class AcceptButtonTrigger : TriggerAction<Button>
    {
        protected override void Invoke(Button button)
        {
            if (button.Text == "ACCEPT")
            {
                button.IsEnabled = false;
                button.IsVisible = false;
            }
            else if(button.Text == "RATE")
            {
                button.IsEnabled = false;
                button.IsVisible = false;
                button.StyleId = "2";
            }

        }
    }
}
